��    V      �     |      x  l   y     �     �             .     )   C     m  	   {  
   �     �     �     �     �     �     �     �     �     �     	  	   	     	     (	  
   8	     C	     O	     X	     m	     {	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     �	     
     
     
     -
     6
     ;
     C
     Q
     Z
     l
     �
     �
  
   �
     �
  z   �
     %  	   2     <     I     N     [  
   h     s     �     �     �     �     �     �  �   �  	   �     �     �     �     �     �  	   �       
   	               5     G     V     j     x     �    �    �  #   �  !   �  #   �       O   %  Q   u     �     �     �               :     Z  6   t     �  *   �  -   �          "     >  !   S     u  !   �     �     �  !   �  #   �  %        E     a     y  '   �     �  !   �     �  2        ?     [     w  '   �     �     �     �  !   �       )   -  &   W     ~  #   �     �     �    �  '   �          4     N     U     s     �  &   �  $   �  &   �  <     :   Y  <   �  :   �  X       e  ,   }     �  !   �     �          &     <     K     `  -   s  )   �  #   �  ,   �  ,     #   I     m                :   5      @   	   J   S      K              ?   7   N      8   *          #   2   D                     O   L          V      (       3   I   E             &               G       .         P         ;             M   R   B   H                  1   /   A   C   >   6   U                     <           $   
       T             0                             4       Q   ,       '      !              %                   +   =       "   9          )       F   -    *Note: Fit counts may be inaccurate as fits marked independently of doctrines may be counted more than once. Add Category Add Doctrine Add Fit Add Fitting Are you sure you wish to delete this doctrine? Are you sure you wish to delete this fit? Are you sure? Cargo Bay Categories Category Category Color Category List Category Name Category is public Close Copy Buy All Copy Fit (EFT) Created Current Icon Dashboard Delete Category Delete Doctrine Delete Fit Description Doctrine Doctrine Description Doctrine Fits Doctrine Information Doctrine List Doctrine Name Doctrines Doctrines Assigned Drone Bay Edit Category Edit Doctrine Edit/Update Fit Fighter Bay Fighter Tubes Fit Fit Information Fit List Fits Fitting Fitting Notes Fittings Fittings Assigned Fittings and Doctrines Groups Groups Assigned High Slots Hull If you do not see the ship type that you would like to use for the icon, add a fit that uses that ship type and try again. Last Updated Low Slots Medium Slots Name New Category New Doctrine No - Close No Categories Found No Doctrines Found No Fits Found No High Slot Items No Low Slot Items No Medium Slot Items No Rig Slot Items Only selected groups will have access to fittings and doctrines in this category. If no groups are selected the category will be visible to all with fittings module access. Rig Slots Select Doctrine Icon Select Doctrines Select Fits Select Groups Service Slots Ship Type Ships SubSystems Submit This action is permanent. Toggle navigation Update Fitting View All Categories View All Fits View Category Yes - Delete Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-21 18:04+0000
Last-Translator: Filipp Chertiev <f@fzfx.ru>, 2023
Language-Team: Russian (https://app.transifex.com/alliance-auth/teams/107430/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 * Заметка: количество оснащений может быть не точным, поскольку оснащения, отмеченные независимо от доктрин, могут быть посчитаны более одного раза. Добавить категорию Добавить доктрину Добавить оснащение Оснастить Вы уверены что хотите удалить эту доктрину? Вы уверены что хотите удалить это оснащение? Вы уверены? Грузовой отсек Категории Категория Цвет категории Список категорий Имя категории Категория является публичной Закрыть Копировать "Купить всё" Копировать оснащение (EFT) Создано Текущая иконка Инфопанель Удалить категорию Удалить доктрину Удалить оснащение Описание Доктрина Описание доктрины Оснащения доктрины Информация доктрины Список доктрин Имя доктрины Доктрины Назначенные доктрины Отсек дронов Править категорию Править доктрину Править/обновить оснащение Отсек файтеров Слоты файтеров Оснащение Информация оснащения Список оснащений Оснащения Оснащение Заметки оснащения Оснащения Назначенные оснащения Оснащения и доктрины Группы Назначенные группы Верхние слоты Корпус Если вы не видите тип корабля, который хотели бы использовать в качестве иконки, то добавьте оснащение, использующее данный тип корабля, и попробуйте снова. Последнее обновление Нижние слоты Средние слоты Имя Новая категория Новая доктрина Нет — закрыть Категории не найдены Доктрины не найдены Оснащений не найдено Нет предметов для верхних слотов Нет предметов для нижних слотов Нет предметов для средних слотов Нет предметов для тюнинг слотов Только выбранные группы будут иметь доступ к оснащениям и доктринам в этой категории. Если не выбрано ни одной группы, то категория будет видима всем, у кого есть доступ к модулю оснащения. Тюнинг слоты Выбрать иконку доктрины Выбрать доктрины Выбрать оснащения Выбрать группы Сервисные слоты Тип корабля Корабли Подсистемы Отправить Это действие необратимо. Переключить навигацию Обновить оснащение Просмотр всех категорий Просмотр всех оснащений Просмотр категории Да — удалить 