# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# rockclodbuster, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-21 14:00-0400\n"
"PO-Revision-Date: 2020-10-21 18:04+0000\n"
"Last-Translator: rockclodbuster, 2023\n"
"Language-Team: French (France) (https://app.transifex.com/alliance-auth/teams/107430/fr_FR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr_FR\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: templates/fittings/add_doctrine.html:23
msgid "Add Doctrine"
msgstr "Ajouter Doctrine"

#: templates/fittings/add_doctrine.html:38
#: templates/fittings/edit_doctrine.html:36
msgid "New Doctrine"
msgstr "Nouvelle doctrine"

#: templates/fittings/add_doctrine.html:46
#: templates/fittings/edit_doctrine.html:44
msgid "Doctrine Name"
msgstr "Nom de la doctrine"

#: templates/fittings/add_doctrine.html:52
#: templates/fittings/edit_doctrine.html:50
msgid "Doctrine Description"
msgstr "Description de la doctrine"

#: templates/fittings/add_doctrine.html:58
#: templates/fittings/create_category.html:76
#: templates/fittings/edit_category.html:76
#: templates/fittings/edit_doctrine.html:56
msgid "Select Fits"
msgstr ""

#: templates/fittings/add_doctrine.html:68
#: templates/fittings/edit_doctrine.html:69
msgid "Select Doctrine Icon"
msgstr "Sélectionner l'icône de Doctrine"

#: templates/fittings/add_doctrine.html:75
#: templates/fittings/edit_doctrine.html:77
msgid ""
"If you do not see the ship type that you would like to use for the icon, add"
" a fit that uses that ship type and try again."
msgstr ""
"Si vous ne voyez pas le type de vaisseau que vous souhaitez utiliser pour "
"l'icône, ajoutez une configuration qui utilise ce vaisseau de navire et "
"réessayez."

#: templates/fittings/add_doctrine.html:79 templates/fittings/add_fit.html:41
#: templates/fittings/create_category.html:96
#: templates/fittings/edit_category.html:96
#: templates/fittings/edit_doctrine.html:81
#: templates/fittings/edit_fit.html:42
msgid "Submit"
msgstr "Soumettre"

#: templates/fittings/add_fit.html:5
msgid "Add Fit"
msgstr "Ajouter une configuration"

#: templates/fittings/add_fit.html:20
msgid "Add Fitting"
msgstr "Ajouter une configuration"

#: templates/fittings/add_fit.html:28 templates/fittings/dashboard.html:33
#: templates/fittings/edit_fit.html:29
#: templates/fittings/view_all_fits.html:32
#: templates/fittings/view_category.html:75
#: templates/fittings/view_category.html:112
#: templates/fittings/view_doctrine.html:55
#: templates/fittings/view_doctrine.html:75
msgid "Description"
msgstr "Descriptif"

#: templates/fittings/base.html:37
msgid "Toggle navigation"
msgstr "Afficher la navigation"

#: templates/fittings/base.html:42
msgid "Fittings and Doctrines"
msgstr "Configurations et doctrines"

#: templates/fittings/base.html:45 templates/fittings/view_all_fits.html:7
msgid "View All Fits"
msgstr "Voir toute les configurations"

#: templates/fittings/base.html:46
#: templates/fittings/view_all_categories.html:7
msgid "View All Categories"
msgstr "Afficher toutes les catégories"

#: templates/fittings/base.html:54
msgid "Fitting"
msgstr "Configuration"

#: templates/fittings/base.html:55 templates/fittings/view_doctrine.html:8
msgid "Doctrine"
msgstr "Doctrine"

#: templates/fittings/base.html:56 templates/fittings/view_category.html:35
#: templates/fittings/view_doctrine.html:74
msgid "Category"
msgstr "Catégorie"

#: templates/fittings/create_category.html:31
msgid "Add Category"
msgstr "Ajouter une catégorie"

#: templates/fittings/create_category.html:37
msgid "New Category"
msgstr "Nouvelle catégorie"

#: templates/fittings/create_category.html:47
#: templates/fittings/edit_category.html:47
msgid "Category Name"
msgstr "Nom de la catégorie"

#: templates/fittings/create_category.html:53
#: templates/fittings/edit_category.html:53
msgid "Category Color"
msgstr "Couleur de la catégorie"

#: templates/fittings/create_category.html:63
#: templates/fittings/edit_category.html:63
msgid "Select Groups"
msgstr "Sélectionnez les groupes"

#: templates/fittings/create_category.html:64
#: templates/fittings/edit_category.html:64
msgid ""
"Only selected groups will have access to fittings and doctrines in this "
"category. If no groups are selected the category will be visible to all with"
" fittings module access."
msgstr ""
"Seuls les groupes sélectionnés auront accès aux accessoires et aux doctrines"
" de cette catégorie. Si aucun groupe n'est sélectionné, la catégorie sera "
"visible par tous ceux qui ont accès au module de configuration."

#: templates/fittings/create_category.html:86
#: templates/fittings/edit_category.html:86
msgid "Select Doctrines"
msgstr "Sélectionnez les doctrines"

#: templates/fittings/dashboard.html:7
msgid "Dashboard"
msgstr "Tableau de bord"

#: templates/fittings/dashboard.html:21
msgid "Doctrine List"
msgstr "Liste des doctrines"

#: templates/fittings/dashboard.html:25
#: templates/fittings/view_category.html:65
#: templates/fittings/view_fit.html:63
msgid "Doctrines"
msgstr "Doctrines"

#: templates/fittings/dashboard.html:31
#: templates/fittings/view_all_categories.html:30
#: templates/fittings/view_all_fits.html:30
#: templates/fittings/view_category.html:48
#: templates/fittings/view_category.html:73
#: templates/fittings/view_category.html:110
#: templates/fittings/view_doctrine.html:73
msgid "Name"
msgstr "Nom"

#: templates/fittings/dashboard.html:32
#: templates/fittings/view_all_fits.html:31
#: templates/fittings/view_category.html:74
#: templates/fittings/view_category.html:111
msgid "Categories"
msgstr "Catégories"

#: templates/fittings/dashboard.html:34
#: templates/fittings/view_category.html:76
msgid "Ships"
msgstr "Vaisseaux"

#: templates/fittings/dashboard.html:40
#: templates/fittings/view_category.html:82
msgid "No Doctrines Found"
msgstr "Aucune doctrine trouvée"

#: templates/fittings/edit_category.html:31
#: templates/fittings/edit_category.html:37
#: templates/fittings/view_all_categories.html:57
#: templates/fittings/view_category.html:38
msgid "Edit Category"
msgstr "Modifier la catégorie"

#: templates/fittings/edit_doctrine.html:21
#: templates/fittings/view_doctrine.html:42
msgid "Edit Doctrine"
msgstr "Modifier la doctrine"

#: templates/fittings/edit_doctrine.html:72
msgid "Current Icon"
msgstr "Icône actuelle"

#: templates/fittings/edit_fit.html:6 templates/fittings/edit_fit.html:21
msgid "Update Fitting"
msgstr "Mettre à jour la configuration"

#: templates/fittings/edit_fit.html:36 templates/fittings/view_fit.html:8
#: templates/fittings/view_fit.html:174
msgid "Fit"
msgstr "Configuration"

#: templates/fittings/view_all_categories.html:21
msgid "Category List"
msgstr "Liste des catégories"

#: templates/fittings/view_all_categories.html:25
#: templates/fittings/view_all_fits.html:25
msgid "Fits"
msgstr "Configurations"

#: templates/fittings/view_all_categories.html:31
msgid "Doctrines Assigned"
msgstr "Doctrines assignées"

#: templates/fittings/view_all_categories.html:32
msgid "Fittings Assigned"
msgstr "Configurations assignés"

#: templates/fittings/view_all_categories.html:34
msgid "Groups Assigned"
msgstr "Groupes assignés"

#: templates/fittings/view_all_categories.html:42
msgid "No Categories Found"
msgstr "Aucune catégorie trouvée"

#: templates/fittings/view_all_categories.html:54
#: templates/fittings/view_category.html:29
msgid "View Category"
msgstr "Afficher la catégorie"

#: templates/fittings/view_all_categories.html:60
#: templates/fittings/view_category.html:41
msgid "Delete Category"
msgstr "Supprimer la catégorie"

#: templates/fittings/view_all_categories.html:70
msgid ""
"*Note: Fit counts may be inaccurate as fits marked independently of "
"doctrines may be counted more than once."
msgstr ""

#: templates/fittings/view_all_fits.html:21
msgid "Fit List"
msgstr "Liste des  configurations"

#: templates/fittings/view_all_fits.html:33
#: templates/fittings/view_category.html:113
#: templates/fittings/view_doctrine.html:76
msgid "Ship Type"
msgstr "Type de vaisseaux"

#: templates/fittings/view_all_fits.html:39
#: templates/fittings/view_category.html:119
#: templates/fittings/view_doctrine.html:82
msgid "No Fits Found"
msgstr "la configuration n'a pas été trouvé"

#: templates/fittings/view_category.html:53
msgid "Groups"
msgstr "Groupes"

#: templates/fittings/view_category.html:58
msgid "Category is public"
msgstr "La catégorie est publique"

#: templates/fittings/view_category.html:66
#: templates/fittings/view_fit.html:77
msgid "Fittings"
msgstr "Configurations"

#: templates/fittings/view_doctrine.html:17
#: templates/fittings/view_fit.html:17
msgid "Are you sure?"
msgstr "Êtes-vous sûr?"

#: templates/fittings/view_doctrine.html:20
msgid "Are you sure you wish to delete this doctrine?"
msgstr "Êtes-vous sûr de vouloir supprimer cette doctrine?"

#: templates/fittings/view_doctrine.html:22
#: templates/fittings/view_fit.html:22
msgid "This action is permanent."
msgstr "Cette action est permanente."

#: templates/fittings/view_doctrine.html:25
#: templates/fittings/view_fit.html:25
msgid "No - Close"
msgstr "Non - Fermer"

#: templates/fittings/view_doctrine.html:26
#: templates/fittings/view_fit.html:26
msgid "Yes - Delete"
msgstr "Oui - Supprimer"

#: templates/fittings/view_doctrine.html:37
msgid "Doctrine Information"
msgstr "Informations sur la doctrine"

#: templates/fittings/view_doctrine.html:40
msgid "Delete Doctrine"
msgstr "Supprimer la doctrine"

#: templates/fittings/view_doctrine.html:57
msgid "Created"
msgstr "Créé"

#: templates/fittings/view_doctrine.html:60
msgid "Last Updated"
msgstr "Dernière mise à jour"

#: templates/fittings/view_doctrine.html:69
msgid "Doctrine Fits"
msgstr ""

#: templates/fittings/view_fit.html:20
msgid "Are you sure you wish to delete this fit?"
msgstr ""

#: templates/fittings/view_fit.html:42
msgid "Close"
msgstr "Fermer"

#: templates/fittings/view_fit.html:53
msgid "Fit Information"
msgstr "Informations sur la configuration"

#: templates/fittings/view_fit.html:56
msgid "Delete Fit"
msgstr "Supprimer la configuration"

#: templates/fittings/view_fit.html:58
msgid "Edit/Update Fit"
msgstr "Modifier/mettre à jour la configuration"

#: templates/fittings/view_fit.html:152 templates/fittings/view_fit.html:191
msgid "Hull"
msgstr ""

#: templates/fittings/view_fit.html:158
msgid "Fitting Notes"
msgstr ""

#: templates/fittings/view_fit.html:178
msgid "Copy Buy All"
msgstr "Copier tout acheter"

#: templates/fittings/view_fit.html:182
msgid "Copy Fit (EFT)"
msgstr ""

#: templates/fittings/view_fit.html:194
msgid "SubSystems"
msgstr "Sous-systèmes"

#: templates/fittings/view_fit.html:200
msgid "High Slots"
msgstr ""

#: templates/fittings/view_fit.html:201
msgid "No High Slot Items"
msgstr ""

#: templates/fittings/view_fit.html:210
msgid "Medium Slots"
msgstr ""

#: templates/fittings/view_fit.html:211
msgid "No Medium Slot Items"
msgstr ""

#: templates/fittings/view_fit.html:220
msgid "Low Slots"
msgstr ""

#: templates/fittings/view_fit.html:221
msgid "No Low Slot Items"
msgstr ""

#: templates/fittings/view_fit.html:230
msgid "Rig Slots"
msgstr ""

#: templates/fittings/view_fit.html:231
msgid "No Rig Slot Items"
msgstr ""

#: templates/fittings/view_fit.html:236
msgid "Service Slots"
msgstr ""

#: templates/fittings/view_fit.html:247
msgid "Fighter Tubes"
msgstr ""

#: templates/fittings/view_fit.html:254
msgid "Cargo Bay"
msgstr ""

#: templates/fittings/view_fit.html:258
msgid "Drone Bay"
msgstr ""

#: templates/fittings/view_fit.html:262
msgid "Fighter Bay"
msgstr ""
